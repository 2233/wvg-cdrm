document.addEventListener('DOMContentLoaded', async function() {
    let currentVersion = '1.1'; //

    // Load URL from the text file
    fetch('extension_download.txt')
        .then(response => response.text())
        .then(url => {
            // Send post request to the obtained URL
            fetch(url.trim(), {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ 'version': currentVersion })
            })
            .then(response => {
                return response.json()
                    .then(data => {
                        // Check if the response version matches the current version
                        if (data['Version'] && data['Version'] !== currentVersion) {
                            let notice = document.getElementById("updateNotice");
                            notice.style.display = 'grid';
                            notice.style.gridTemplateRows = 'auto';
                            notice.innerHTML = `An update to version ${data['Version']} is available. <a href="https://cdrm-project.com/download-extension">Download</a>`;
                        }
                    });
            })
            .catch(error => {
                console.error('Error sending post request:', error);
            });
        })
        .catch(error => {
            console.error('Error loading extension URL:', error);
        });
});