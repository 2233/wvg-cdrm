let psshs=chrome.extension.getBackgroundPage().psshs;
let requests=chrome.extension.getBackgroundPage().requests;
let pageURL=chrome.extension.getBackgroundPage().pageURL;
let clearkey=chrome.extension.getBackgroundPage().clearkey;

async function guess(){
    // Be patient!
    document.body.style.cursor = "wait";
    document.getElementById("guess").disabled = true;

    // Init Pyodide
    let pyodide = await loadPyodide();
    await pyodide.loadPackage(["certifi-2024.2.2-py3-none-any.whl","charset_normalizer-3.3.2-py3-none-any.whl","construct-2.8.8-py2.py3-none-any.whl","idna-3.6-py3-none-any.whl","packaging-23.2-py3-none-any.whl","protobuf-4.24.4-cp312-cp312-emscripten_3_1_52_wasm32.whl","pycryptodome-3.20.0-cp35-abi3-emscripten_3_1_52_wasm32.whl","pymp4-1.4.0-py3-none-any.whl","pyodide_http-0.2.1-py3-none-any.whl","pywidevine-1.8.0-py3-none-any.whl","requests-2.31.0-py3-none-any.whl","urllib3-2.2.1-py3-none-any.whl"].map(e => "wheels/" + e));

    let json_dict = {
        'PSSH': document.getElementById('pssh').value,
        'License URL': requests[userInputs['license']]['url'],
        'Headers': requests[userInputs['license']]['headers'],
        'JSON': requests[userInputs['license']]['body'],
        'Scheme': document.getElementById("scheme").value,
        'Proxy': document.getElementById("proxy").value,
    };

    // Set request options
    let requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(json_dict)
    };

    // Load URL from the text file
    fetch('extension_url.txt')
    .then(response => response.text())
    .then(url => {
        // Send post request
        fetch(url.trim(), requestOptions)
        .then(response => {
            return response.json()
            .then(data => {
                // Grab the message
                document.getElementById('result').value = data['Message'];

                // Save history
                let historyData = {
                    PSSH: document.getElementById('pssh').value,
                    KEYS: document.getElementById("result").value.trim().split("\n"),
                };
                chrome.storage.local.set({ [pageURL]: historyData }, function () {
                    // Callback function after history data is saved (if needed)
                });
            });
        })
        .catch(error => {
            console.error('Error sending post request:', error);
        });
    });

    // All Done!
    document.body.style.cursor = "auto";
    document.getElementById("guess").disabled = false;
}

function copyResult(){
    this.select();
    navigator.clipboard.writeText(this.value);
}

window.corsFetch = (u, m, h, b) => {
    return new Promise((resolve, reject) => {
        chrome.tabs.query({ url:pageURL }, (tabs) => {
            chrome.tabs.sendMessage(tabs[0].id, {type:"FETCH", u:u, m:m, h:h, b:b}, (res) => {
                resolve(res)
            })
        })
    })
}

async function autoSelect(){
    let selectRules = await fetch("selectRules.conf").then((r)=>r.text());
    //Remove blank lines, comment-outs, and trailing spaces at the end of lines
    selectRules = selectRules.replace(/\n^\s*$|\s*\/\/.*|\s*$/gm, "");
    selectRules = selectRules.split("\n").map(row => row.split("$$"));
    for(var item of selectRules){
        let search = requests.map(r => r['url']).findIndex(e => e.includes(item[0]));
        if(search>=0){
            if(item[1]) document.getElementById("scheme").value = item[1];
            requestList.children[search].click();
            break;
        }
    }
    if(psshs.length==1){
        document.getElementById('pssh').value=psshs[0];
    }
    if(requests.length==1){
        requestList.children[0].click();
    }
}

if(psshs.length!=0){
    document.addEventListener('DOMContentLoaded', function() {
        document.getElementById('noEME').style.display='none';
        document.getElementById('home').style.display='block';
        document.getElementById('guess').addEventListener("click", guess);
        document.getElementById('result').addEventListener("click", copyResult);
        drawList(psshs,'psshSearch','psshList','pssh');
        drawList(requests.map(r => r['url']),'requestSearch','requestList','license');
        autoSelect();
		autoMPD(); // autoMPD ADD+
    });
} else if(clearkey) {
    document.getElementById('noEME').style.display='none';
    document.getElementById('ckHome').style.display='block';
    document.getElementById('ckResult').value=clearkey;
    document.getElementById('ckResult').addEventListener("click", copyResult);
}

// autoMPD ADD+
function autoMPD() {
    chrome.storage.sync.get('lastUrl', function (data) {
        let url = "";
        if (data.lastUrl) url = data.lastUrl;
        document.getElementById("last_mpd").value = url;
    });
}

// SaveMPD ADD+
function SaveMPD() {
    if (document.getElementById("last_mpd").value.length > 0) {
        let copyText = document.getElementById("last_mpd").value;
        navigator.clipboard.writeText(copyText);
    }
}

// MPDButton ADD+
document.getElementById('MPDButton').addEventListener("click", SaveMPD);
